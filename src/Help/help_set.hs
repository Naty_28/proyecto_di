<?xml version="1.0" encoding='utf-8' ?> 
<!DOCTYPE helpset 
PUBLIC "-//Sun Microsystems Inc.//DTD JavaHelp HelpSet Version 1.0//EN" 
"http://java.sun.com/products/javahelp/helpset_1_0.dtd"> 
<helpset version="1.0"> 
    <title>Recetario</title> 
    <maps> 
        <homeID>aplicacion</homeID> <!-- Página por defecto al mostrar la ayuda --> 
        <mapref location="map_file.jhm"/> <!-- Que mapa deseamos --> 
    </maps> 
    <!-- La tabla de contenidos -->
    <view>
        <name>Contenidos</name>
        <label>Tabla de contenidos</label>
        <type>javax.help.TOCView</type>
        <data>toc.xml</data>
    </view>
    <!-- El indice -->
    <view>
        <name>Indice</name>
        <label>Indice</label>
        <type>javax.help.IndexView</type>
        <data>indice.xml</data>
    </view>
    <!-- La pestana de busqueda -->
    <view>
        <name>Buscar</name>
        <label>Buscar</label>
        <type>javax.help.SearchView</type>
        <data engine="com.sun.java.help.search.DefaultSearchEngine">
            JavaHelpSearch
        </data>
    </view>
    <!-- Pestaña de favoritos-->
    <view>
      <name>Favorites</name>
      <label>Favorites</label>
      <type>javax.help.FavoritesView</type>
   </view>
</helpset>
