/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import servidorobj.Persona;

/**
 *
 * @author Roberto
 */
public class HiloServidor extends Thread {

    Socket cliente;

    public HiloServidor(Socket socket) {
        this.cliente = socket;
    }

    public void run() {
        try {
            //Se prepara un flujo de salida para escribir objetos en stream
            ObjectOutputStream outObjeto
                    = new ObjectOutputStream(
                            cliente.getOutputStream());
            ObjectInputStream inObjeto
                    = new ObjectInputStream(
                            cliente.getInputStream()); //para leer objetos del stream
            //Objeto persona que quiero enviar:
            Persona persona = new Persona("Roberto", 40);
            //envio el objeto persona por el stream
            outObjeto.writeObject(persona);
            System.out.println("Enviando los siguientes datos de la persona: "
                    + persona.getNombre() + " edad = " + persona.getEdad());
            /*Cuando el cliente  haya modificado los datos de la persona, los
            voy a leer del stream. */
            Persona datosNuevos = (Persona) inObjeto.readObject(); //arroja ClassNotFoundException
            System.out.println("Los nuevos datos de la persona son: "
                    + datosNuevos.getNombre() + " edad " + datosNuevos.getEdad());
            //Cierro Stream y socket
            outObjeto.flush();
            outObjeto.close();
            inObjeto.close();

            cliente.close();
        } catch (IOException ex) {
            Logger.getLogger(ServidorObj.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServidorObj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
