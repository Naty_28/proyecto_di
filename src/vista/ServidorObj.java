/*
 * SErvidor: crea un objeto de tipo persona u lo envía al cliente
El cliente puede cambiar los datos del objeto y enviarlos al servidor.
 */
package vista;

import java.io.*;
import java.net.*;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author solas
 */
public class ServidorObj extends Thread{

    /**
     * @param args the command line arguments
     */
    public void run() {
        try {
            int puerto = 6600;
            ServerSocket servidor = new 
                    ServerSocket(puerto);
            while (true)   {      
                System.out.println("Servidor esperando al cliente");
                Socket cliente = servidor.accept();
                HiloServidor hs = new HiloServidor (cliente);
                hs.start();
            }

    
        } catch (IOException ex) {
            Logger.getLogger(ServidorObj.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
