/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import vista.Conexion;

/**
 *
 * @author natal
 */
public class ProductoModelo {
    
    private ArrayList<Producto> productos;
    Connection con;

    
    
    /**
     * Constructor sin parámetros que instancia el ArrayList productos
     */
    public ProductoModelo() {
        productos = new ArrayList<>();
    }
   
    public ArrayList<Producto> getProductos(){
        return productos;
    }
    
    public void setProductos(ArrayList<Producto> productos){
        this.productos = productos;
    }
    
    public void AñadirProducto(Producto producto) throws SQLException{
        
        this.productos.add(producto);
        
        con= Conexion.conexionMySQL();
        
        //Inserto el Producto creado previamente
        //primero inserto su color
        
        String consultacolorSQL = "INSERT INTO item (p_item, a_producto, a_color, a_talla, pvp, stock, imagen) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement sentenciaSQL = con.prepareStatement(consultacolorSQL);
        sentenciaSQL.setInt(1, producto.getId());
        sentenciaSQL.setString(2, producto.getTipo());
        sentenciaSQL.setString(3, producto.getCategoria());
        sentenciaSQL.setString(4, producto.getColor());
        sentenciaSQL.setString(5, producto.getTalla());
        sentenciaSQL.setString(6, producto.getComposicion());
        sentenciaSQL.setDouble(7, producto.getPvp());
        sentenciaSQL.setInt(8, producto.getStock());
        sentenciaSQL.setString(9, producto.getImagen());

        int resultado = sentenciaSQL.executeUpdate();
        System.out.println("Se ha creado el producto correctamente");
        con.close();
        //Finalización inserción.
    }
}
