/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author natal
 */
public class Producto {
    private int Id;
    private String Tipo;
    private String Categoria;
    private String Color;
    private String Talla;
    private int Stock;
    private String Composicion;
    private double Pvp;
    private String Imagen;
    
    /**
     * Constructor sin parámetros
     */
    public Producto() {
    }

    /**
     * Constructor con parámetros
     * 
     * @param Id identificador del producto
     * @param Nombre su nombre
     * @param Categoria debe ser Ropa, Regalos, Complementos, Papelería, Decoración
     * @param Color deben ser Azul, Rosa, Negro, Blanco, Decorado
     * @param Talla deben ser S, M, L, Unica
     * @param Stock el stock de cuantos productos hay
     * @param Composicion la composicion de cada producto ofrecido
     * @param Pvp el precio de cada producto
     * @param Imagen la imagen del producto
     */   
    
    
    public Producto(int Id, String Tipo, String Categoria, String Color, String Talla, int Stock, String Composicion, double Pvp, String Imagen) {
        this.Id = Id;
        this.Tipo = Tipo;
        this.Categoria = Categoria;
        this.Color = Color;
        this.Talla = Talla;
        this.Stock = Stock;
        this.Composicion = Composicion;
        this.Pvp = (double) Pvp;
        this.Imagen = Imagen;
    }

  

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String Color) {
        this.Color = Color;
    }

    public String getTalla() {
        return Talla;
    }

    public void setTalla(String Talla) {
        this.Talla = Talla;
    }

    public int getStock() {
        return Stock;
    }

    public void setStock(int Stock) {
        this.Stock = Stock;
    }

    public String getComposicion() {
        return Composicion;
    }

    public void setComposicion(String Composicion) {
        this.Composicion = Composicion;
    }

    public double getPvp() {
        return Pvp;
    }

    public void setPvp(int Pvp) {
        this.Pvp = Pvp;
    }

    public String getImagen() {
        return Imagen;
    }

    public void setImagen(String Imagen) {
        this.Imagen = Imagen;
    }
    
         
    
    
}
