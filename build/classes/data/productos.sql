DROP DATABASE IF EXISTS productos;
CREATE DATABASE productos;
USE productos;

CREATE TABLE color (
    p_color int(2) not null PRIMARY KEY,
    nombre varchar(100)
    )ENGINE=INNODB;
    
CREATE TABLE talla (
    p_talla int(2) not null PRIMARY KEY,
    nombre varchar(100)
    )ENGINE=INNODB;

CREATE TABLE categoria (
    p_categoria int(2) not null PRIMARY KEY,
    nombre varchar(100)
    )ENGINE=INNODB;
    
     CREATE TABLE iva (
		p_iva int(2) not null PRIMARY KEY,
        tipo varchar(50)
    )ENGINE = INNODB;

CREATE TABLE producto (
    p_producto int(2) not null PRIMARY key,
    a_categoria int(2),
    nombre varchar(100),
    composicion varchar(100),
    foreign key (a_categoria) references categoria(p_categoria)
    on delete restrict on update cascade
    )ENGINE=INNODB;

CREATE TABLE item (
    p_item int(2) not null PRIMARY key,
    a_producto int(2) not null,
    a_color int(2),
    a_talla int(2),
    pvp decimal(2,2),
    stock int(2),
    imagen varchar(250),
    foreign key (a_producto) references producto(p_producto)
    on delete restrict on update cascade ,
    foreign key (a_color) references color(p_color)
    on delete restrict on update cascade ,
    foreign key (a_talla) references talla(p_talla)
    on delete restrict on update cascade 
    )ENGINE=INNODB;
    
    CREATE TABLE cliente (
		p_cliente int(2) not null PRIMARY KEY,
        nombre varchar(50),
        apellido varchar(50),
        telefono int(9),
        email varchar(50),
        contrasena int(5)
    )ENGINE = INNODB;
    
     CREATE TABLE pedido (
		p_pedido int(2) not null PRIMARY KEY,
        a_cliente int(2),
        fecha_pedido date,
        foreign key (a_cliente) references cliente(p_cliente)
        on delete restrict on update cascade
    )ENGINE = INNODB;
    
    CREATE TABLE detalle_pedido (
		p_det_pedido int (2) not null PRIMARY KEY,
        a_item int (2),
        a_pedido int (2),
        cantidad int(3),
        foreign key (a_item) references item(p_item)
        on delete restrict on update cascade,
        foreign key (a_pedido) references pedido(p_pedido)
        on delete restrict on update cascade
    )ENGINE = INNODB;
    
   
    CREATE TABLE factura (
		p_factura int(2) not null PRIMARY KEY,
        a_cliente int(2),
        a_iva int(2),
        fecha_factura date,
        foreign key (a_cliente) references cliente(p_cliente)
        on delete restrict on update cascade,
        foreign key (a_iva) references iva(p_iva)
        on delete restrict on update cascade
    )ENGINE = INNODB;
    
    CREATE TABLE detalle_factura (
		p_det_factura int (2) not null PRIMARY KEY,
        a_item int (2),
        a_factura int (2),
        cantidad int(3),
        foreign key (a_item) references item(p_item)
        on delete restrict on update cascade,
        foreign key (a_factura) references factura(p_factura)
        on delete restrict on update cascade
    )ENGINE = INNODB;
    
   
    /*------------------------Color-------------*/
    INSERT INTO color VALUES (01,'Azul');
    INSERT INTO color VALUES (02,'Rosa');
    INSERT INTO color VALUES (03,'Negro');
    INSERT INTO color VALUES (04,'Blanco');
    INSERT INTO color VALUES (05,'decorado');
    
    /*----------------------Talla--------------*/
    INSERT INTO talla VALUES (01,'s');
    INSERT INTO talla VALUES (02,'m');
    INSERT INTO talla VALUES (03,'l');
    INSERT INTO talla VALUES (04,'unica');
    
    /*----------------------Categoria--------------*/
    INSERT INTO categoria VALUES (01,'ropa');
    INSERT INTO categoria VALUES (02,'papelería');
    INSERT INTO categoria VALUES (03,'decoración');
    INSERT INTO categoria VALUES (04,'regalos');
    INSERT INTO categoria VALUES (05,'complementos');
    
    /*----------------------------Iva-----------------*/
    INSERT INTO iva VALUES (01,'21');
    INSERT INTO iva VALUES (02,'18');
    INSERT INTO iva VALUES (03,'16');
    
    /*-------------------Producto--------------------*/
    INSERT INTO producto VALUES (01, 01,'sudadera',  'algodon');
    INSERT INTO producto VALUES (02, 02,'agenda', 'papel');
    INSERT INTO producto VALUES (03, 03,'poster', 'papel');
    INSERT INTO producto VALUES (04, 04,'taza',  'porcelana');
    INSERT INTO producto VALUES (05, 05,'funda_movil', 'plastico');
    INSERT INTO producto VALUES (06, 01,'camiseta',  'algodon');
    INSERT INTO producto VALUES (07, 02,'block de notas', 'papel');
    INSERT INTO producto VALUES (08, 03,'luces', 'plastico');
    INSERT INTO producto VALUES (09, 04,'tarjeta',  'papel');
    INSERT INTO producto VALUES (10, 05,'funda_tablet',  'plastico');
    
    /*-----------------Item-------------------*/
    
    INSERT INTO item VALUES (01, 01, 02, 01,20.99, 2,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\Ropa\sudadera_blanca.jpg');
    INSERT INTO item VALUES (02, 02, 01, 04,10.95, 3, 'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\papeleria\agenda_dec1.jpg');
    INSERT INTO item VALUES (03, 03, 01, 04,5.90, 2,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\decoracion\poster_2.jpg');
    INSERT INTO item VALUES (04, 04, 05, 04, 10.00,5,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\regalos\taza1.jpg');
    INSERT INTO item VALUES (05, 01, 03, 02, 6.00, 5,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\ropa\sudadera_di2.jpg');
    INSERT INTO item VALUES (06, 01, 05, 03, 15.99,6,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\ropa\sudadera_di1.jpg');
    INSERT INTO item VALUES (07, 02, 02, 04, 4.95,2,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\papeleria\agenda_dec2.jpg');
    INSERT INTO item VALUES (08, 08, 02, 04, 8.00,3,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\decoracion\luces_flor.jpg');
    INSERT INTO item VALUES (09, 09, 05, 04, 3.95,8,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\regalos\tar4.jpg');
    INSERT INTO item VALUES (10, 10, 01, 04, 12.95,5,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\Complementos\carcasa_tablet1.jpg');
    INSERT INTO item VALUES (11, 05, 02, 04,13.95, 5,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\Complementos\carcasa_movil2.jpg');
    INSERT INTO item VALUES (12, 03, 05, 04, 6.95,6,'C:\Users\natal\Desktop\proyecto\src\recursos\img_subidas\decoracion\poster_6.jpg');
    
    /*-------------------------Cliente----------------*/

    INSERT INTO cliente VALUES (01, 'David', 'Solis', 695541230, 'david@gmail.com',12345);
    INSERT INTO cliente VALUES (02, 'Ana', 'Perez', 653201412,'ana@gmail.com', 98745);
    INSERT INTO cliente VALUES (03, 'Lucia', 'Cano', 659874521, 'lucia@gmail.com', 45612);
    
    /*-------------------Pedido-----------------*/
    INSERT INTO pedido VALUES (01,02,03/02/2006);
    INSERT INTO pedido VALUES (02,03,25/12/2017);
    INSERT INTO pedido VALUES (03,01,10/09/2016);
    
    /*---------------------Detalle_Pedido----------------*/
    INSERT INTO detalle_pedido VALUES (01,06,03,2);
    INSERT INTO detalle_pedido VALUES (02,04,01,1);
    INSERT INTO detalle_pedido VALUES (03,12,02,1);
    
    /*--------------------Factura-----------------------------*/
    INSERT INTO factura VALUES (01,02,02,03/02/2006);
    INSERT INTO factura VALUES (02,03,01,25/12/2017);
    INSERT INTO factura VALUES (03,01,01,10/09/2016);
    
    /*-------------------------Detalle_factura-------------------*/
    INSERT INTO detalle_factura VALUES (01,06,03,2);
    INSERT INTO detalle_factura VALUES (02,04,01,1);
    INSERT INTO detalle_factura VALUES (03,12,02,1);
    
    